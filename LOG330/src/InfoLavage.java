
public class InfoLavage {

	private Boolean eauFroide;
	private int tempsLavage, tempsEssorage;
	private int cadenceLavage, cadenceEssorage;
	private int niveauEauMinimal, niveauEauMaximal;
	
	private InfoLavage(){
		
	}
	
	public static InfoLavage CreerInfosCoton() {
		InfoLavage info = new InfoLavage();
		info.eauFroide = true;
		info.tempsLavage = 45;
		info.tempsEssorage = 10;
		info.cadenceLavage = 10;
		info.cadenceEssorage = 20;
		info.niveauEauMaximal = 10;
		info.niveauEauMinimal = 1;
		return info;
	}

	public static InfoLavage CreerInfosSynthetique() {
		InfoLavage info = new InfoLavage();
		info.eauFroide = true;
		info.tempsLavage = 30;
		info.tempsEssorage = 5;
		info.cadenceLavage = 15;
		info.cadenceEssorage = 10;
		info.niveauEauMaximal = 10;
		info.niveauEauMinimal = 1;
		return info;
	}
	
	public static InfoLavage CreerInfosRugueux() {
		InfoLavage info = new InfoLavage();
		info.eauFroide = true;
		info.tempsLavage = 45;
		info.tempsEssorage = 10;
		info.cadenceLavage = 8;
		info.cadenceEssorage = 20;
		info.niveauEauMaximal = 10;
		info.niveauEauMinimal = 1;
		return info;
	}
	
	public static InfoLavage CreerInfosTrempageEssorage() {
		InfoLavage info = new InfoLavage();
		info.eauFroide = true;
		info.tempsLavage = 10;
		info.tempsEssorage = 15;
		info.cadenceLavage = 0;
		info.cadenceEssorage = 30;
		info.niveauEauMaximal = 10;
		info.niveauEauMinimal = 10;
		return info;
	}
	
	public static InfoLavage CreerInfosDesinfection() {
		InfoLavage info = new InfoLavage();
		info.eauFroide = false;
		info.tempsLavage = 45;
		info.tempsEssorage = 10;
		info.cadenceLavage = 10;
		info.cadenceEssorage = 20;
		info.niveauEauMaximal = 5;
		info.niveauEauMinimal = 5;
		return info;
	}
	
	public Boolean utiliseEauFroide() {
		return eauFroide;
	}
	public int getTempsLavage() {
		return tempsLavage;
	}

	public int getTempsEssorage() {
		return tempsEssorage;
	}

	public int getCadenceLavage() {
		return cadenceLavage;
	}

	public int getCadenceEssorage() {
		return cadenceEssorage;
	}

	public int getNiveauEauMinimal() {
		return niveauEauMinimal;
	}

	public int getNiveauEauMaximal() {
		return niveauEauMaximal;
	}

}
