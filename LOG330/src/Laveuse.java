import java.util.Timer;
import java.util.TimerTask;

public class Laveuse {
	
	public enum Cycles {
		COTON, SYNTHETIQUE, RUGUEUX, TREMPAGE_ESSORAGE, DESINFECTION
	}
	
	public enum Etat {
		ARRETE, MARCHE, REINITIALISATION
	}
	
	public enum Etape {
		INACTIF, REMPLISSAGE, LAVAGE, VIDANGE, ESSORAGE
	}
	
	private PortSerie port;
	private InfoLavage infos;
	private int niveauEauCible=1;
	private Etat etatCourant;
	private Cycles cycleSelectionne;
	private Etape etapeCourante;
	private Timer timerAvancement;
	private Timer mainTimer;
	private MainView view;
	
	public Laveuse(PortSerie port, MainView view) {
		timerAvancement = new Timer();
		this.view = view;
		this.port = port;
		etatCourant = Etat.ARRETE;
		etapeCourante = Etape.INACTIF;
		niveauEauCible = choisirCycle(Cycles.COTON);
		mainTimer = new Timer();
		mainTimer.schedule(new TimerTask(){
			public void run() {
				verifierMemoire();
			}
		}, 0, 1000);
	}
	
	public void verifierMemoire() {
		if(etatCourant == Etat.MARCHE)
		{
			if(!port.estEnMarche()){
				terminer();
			}
			return;
		}
		else if(etatCourant == Etat.ARRETE){
			
			niveauEauCible = port.getNiveauEauVoulu();
			
			choisirCycle(port.getCycle());
			
			if(port.estEnMarche()){
				demarrerCycle();
			}
		}
		
	}

	public Cycles getCycle(){
		return cycleSelectionne;
	}
	
	public int choisirCycle(Cycles cycle) {
		if(cycleSelectionne != cycle && etatCourant == Etat.ARRETE)
		{
			cycleSelectionne = cycle;
			switch(cycle) {
				case COTON:
					infos = InfoLavage.CreerInfosCoton();
					break;
				case SYNTHETIQUE:
					infos = InfoLavage.CreerInfosSynthetique();
					break;
				case RUGUEUX:
					infos = InfoLavage.CreerInfosRugueux();
					break;
				case TREMPAGE_ESSORAGE:
					infos = InfoLavage.CreerInfosTrempageEssorage();
					break;
				case DESINFECTION:
					infos = InfoLavage.CreerInfosDesinfection();
					break;
			}
			
			if(niveauEauCible < infos.getNiveauEauMinimal()){
				niveauEauCible = infos.getNiveauEauMinimal();
			}
			else {
				niveauEauCible = infos.getNiveauEauMaximal();
			}
		}
		
		return niveauEauCible;
		
	}
	
	public int augmenterNiveauEau() {
		if(etatCourant == Etat.ARRETE)
		{
			niveauEauCible++;
			
			if(niveauEauCible < infos.getNiveauEauMinimal()){
				niveauEauCible = infos.getNiveauEauMinimal();
			}
			
			else if(niveauEauCible > infos.getNiveauEauMaximal()){
				niveauEauCible = infos.getNiveauEauMinimal();
			}
		}
		
		return niveauEauCible;
	}

	public Etat getEtat(){
		return etatCourant;
	}
	
	public int getNiveauEauCible() {
		return niveauEauCible;
	}

	private void demarrerCycle(){
		etapeCourante = Etape.REMPLISSAGE;
		view.updateEtape(etapeCourante);
		etatCourant = Etat.MARCHE;
		port.ouvrirEauChaude();
		if(infos.utiliseEauFroide()) {
			port.ouvrirEauFroide();
		}
		
		//Ins�rer le savon � 20% de remplissement
		timerAvancement.schedule(new TimerTask() {
			public void run() {
				verifierRemplissage((int)(niveauEauCible * 0.2), new Runnable(){
					public void run(){
						port.ouvrirValveSavon();
						timerAvancement.schedule(new TimerTask() {
							public void run() {
								//D�marrer le lavage quand la cuve est pleine.
								verifierRemplissage(niveauEauCible, new Runnable(){
									public void run() {
										port.ouvrirValveJavellisant();
										port.fermerEauFroide();
										port.fermerEauChaude();
					                	laver();						
									}
				                });
							}
						}, 0, 1000);
					}
				});
			}

        }, 0, 1000);
	}
	
	private void verifierRemplissage(int niveau, Runnable task) {
		if(port.getCurrentNiveauEau() >= niveau){
			timerAvancement.cancel();
			timerAvancement = new Timer();
			task.run();
		}
		else{
			System.out.println("Attend que la laveuse soit pleine au niveau " + niveau + ".");
		}
	}
	
	private void laver(){
		timerAvancement.cancel();
		timerAvancement = new Timer();
		etapeCourante = Etape.LAVAGE;
		view.updateEtape(etapeCourante);
		
		port.setCadenceMoteur(infos.getCadenceLavage());
		port.setTempsRestant(infos.getTempsLavage());
		view.updateTemps();
		
		timerAvancement.schedule(new TimerTask(){
			public void run(){
				verifierTempsRestant(new Runnable() {
					public void run(){
						port.setCadenceMoteur(0);
						vider();
					}
				});
			}
		}, 0, 1000);
	}
	
	private void verifierTempsRestant(Runnable task){
		int restant = port.getTempsRestant();
		if(restant > 0){
			if(etapeCourante == Etape.LAVAGE && restant <= 5){
				port.ouvrirValveAssouplisseur();
			}
			port.setTempsRestant(restant - 1);
			System.out.println("Attend que l'�tape se termine.");
			view.updateTemps();
		}
		else{
			timerAvancement.cancel();
			timerAvancement = new Timer();
			view.updateTemps();
			task.run();
		}
	}
	
	private void vider(){
		etapeCourante = Etape.VIDANGE;
		view.updateEtape(etapeCourante);
		port.demarrerPompe();
		
		timerAvancement.schedule(new TimerTask() {
			public void run() {
				verifierVidange(new Runnable(){
					public void run() {
						port.arreterPompe();
						essorer();
					}
				});
			}			
		}, 0, 1000);
	}
	
	private void verifierVidange(Runnable task){
		if(port.getCurrentNiveauEau() <= 0){
			timerAvancement.cancel();
			timerAvancement = new Timer();
			task.run();
		}
		else{
			System.out.println("Attend que la laveuse soit vide.");
		}
	}
	
	private void essorer(){
		etapeCourante = Etape.ESSORAGE;
		view.updateEtape(etapeCourante);
		port.setCadenceMoteur(infos.getCadenceEssorage());
		port.setTempsRestant(infos.getTempsEssorage());
		view.updateTemps();
		timerAvancement.schedule(new TimerTask() {
			public void run(){
				verifierTempsRestant(new Runnable(){
					public void run(){
						terminer();
					}
				});
			}
		},0,1000);
	}
	
	private void terminer(){
		etatCourant = Etat.REINITIALISATION;
		timerAvancement.cancel();
		timerAvancement = new Timer();
		port.fermerValveAssouplisseur();
		port.fermerValveJavellisant();
		port.fermerValveSavon();
		port.setCadenceMoteur(0);
		port.fermerEauFroide();
		port.fermerEauChaude();
		port.setTempsRestant(0);
		view.updateTemps();
		if(port.getNiveauEauVoulu() > 0){
			etapeCourante = Etape.VIDANGE;
			view.updateEtape(etapeCourante);
			timerAvancement.schedule(new TimerTask(){
				public void run() {
					verifierVidange(new Runnable(){
						public void run(){
							//TODO: what do we do next
							port.arreterPompe();
							etapeCourante = Etape.INACTIF;
							view.updateEtape(etapeCourante);
							etatCourant = Etat.ARRETE;
							port.arreter();
						}
					});
				}
			},0,1000);
		}
		else{
			etapeCourante = Etape.INACTIF;
			view.updateEtape(etapeCourante);
			etatCourant = Etat.ARRETE;
			port.arreter();
		}
	}
	
}
