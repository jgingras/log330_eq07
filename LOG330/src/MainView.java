import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

import java.awt.GridLayout;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Canvas;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JProgressBar;
import javax.swing.JToggleButton;
import java.awt.FlowLayout;

public class MainView extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JProgressBar progressBar;
	private JPanel panel_1 = new JPanel();
	private JToggleButton boutonSynthetique, boutonCoton, boutonTrempageEssorage,
						  boutonRugueux, boutonDesinfection;
	private JLabel labelTemps, labelEtape;
	private ButtonGroup boutonsCycles = new ButtonGroup();
	private Laveuse laveuse;
	private MemoryContent memContent;
	private PortSerie port;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainView frame = new MainView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainView() {
		memContent = new MemoryContent();
		memContent.setVisible(true);
		port = new PortSerie(memContent);
		laveuse = new Laveuse(port, this);
		
		setTitle("Machine \u00E0 laver");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 751, 260);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(2, 0, 0, 0));
		
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		boutonCoton = new JToggleButton("Coton");
		boutonCoton.setSelected(true);
		boutonCoton.setBounds(13, 23, 129, 60);
		boutonCoton.setAlignmentX(Component.CENTER_ALIGNMENT);
		boutonCoton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				changerCycle(Laveuse.Cycles.COTON);
			}
		});
		panel_1.add(boutonCoton);
		boutonsCycles.add(boutonCoton);
		
		boutonSynthetique = new JToggleButton("Synth\u00E9tique");
		boutonSynthetique.setBounds(297, 23, 129, 60);
		boutonSynthetique.setAlignmentX(Component.CENTER_ALIGNMENT);
		boutonSynthetique.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				changerCycle(Laveuse.Cycles.SYNTHETIQUE);
			}
		});
		panel_1.add(boutonSynthetique);
		boutonsCycles.add(boutonSynthetique);
		
		boutonRugueux = new JToggleButton("Rugueux");
		boutonRugueux.setBounds(155, 23, 129, 60);
		boutonRugueux.setAlignmentX(Component.CENTER_ALIGNMENT);
		boutonRugueux.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				changerCycle(Laveuse.Cycles.RUGUEUX);
			}
		});
		panel_1.add(boutonRugueux);
		boutonsCycles.add(boutonRugueux);
		
		boutonDesinfection = new JToggleButton("D\u00E9sinfection");
		boutonDesinfection.setBounds(439, 23, 129, 60);
		boutonDesinfection.setAlignmentX(Component.CENTER_ALIGNMENT);
		boutonDesinfection.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				changerCycle(Laveuse.Cycles.DESINFECTION);
			}
		});
		panel_1.add(boutonDesinfection);
		boutonsCycles.add(boutonDesinfection);
		
		boutonTrempageEssorage = new JToggleButton("Trempage/Essorage");
		boutonTrempageEssorage.setBounds(581, 23, 129, 60);
		boutonTrempageEssorage.setAlignmentX(Component.CENTER_ALIGNMENT);
		boutonTrempageEssorage.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				changerCycle(Laveuse.Cycles.TREMPAGE_ESSORAGE);
			}
		});
		panel_1.add(boutonTrempageEssorage);
		boutonsCycles.add(boutonTrempageEssorage);
		
		JPanel panel = new JPanel();
		contentPane.add(panel);
		panel.setLayout(null);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(30, 23, 129, 60);
		panel.add(panel_2);
		panel_2.setLayout(null);
		
		JButton boutonNiveauEau = new JButton("Niveau d'eau");
		boutonNiveauEau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changerNiveauEau();
			}
		});
		boutonNiveauEau.setBounds(0, 0, 108, 60);
		panel_2.add(boutonNiveauEau);
		
		progressBar = new JProgressBar();
		progressBar.setMinimum(-1);
		progressBar.setValue(laveuse.getNiveauEauCible());
		progressBar.setMaximum(10);
		progressBar.setBounds(112, 0, 17, 60);
		panel_2.add(progressBar);
		progressBar.setOrientation(SwingConstants.VERTICAL);
		
		JButton boutonDepart = new JButton("D\u00E9part");
		boutonDepart.setBounds(189, 23, 129, 60);
		boutonDepart.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				demarrer();
			}
		});
		panel.add(boutonDepart);
		
		JButton boutonArret = new JButton("Arr\u00EAt");
		boutonArret.setBounds(563, 23, 129, 60);
		boutonArret.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				arreter();
			}
		});
		panel.add(boutonArret);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBounds(348, 23, 185, 60);
		panel.add(panel_3);
		panel_3.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JPanel panel_4 = new JPanel();
		panel_3.add(panel_4);
		
		JLabel lblNewLabel = new JLabel("\u00C9tape en cours : ");
		panel_4.add(lblNewLabel);
		
		labelEtape = new JLabel("Arr\u00EAt\u00E9e");
		panel_4.add(labelEtape);
		
		JPanel panel_5 = new JPanel();
		panel_3.add(panel_5);
		
		JLabel lblNewLabel_2 = new JLabel("Temps restant : ");
		panel_5.add(lblNewLabel_2);
		
		labelTemps = new JLabel("0");
		panel_5.add(labelTemps);
		
		updateEtape(Laveuse.Etape.INACTIF);
	}
	
	private void changerCycle(Laveuse.Cycles cycle) {
		if(laveuse.getEtat() == Laveuse.Etat.ARRETE){
			int nouveauNiveau = laveuse.choisirCycle(cycle);
			progressBar.setValue(nouveauNiveau);
			port.setNiveauEauVoulu(nouveauNiveau);
			port.setCycle(cycle);
		}
		else{
			Laveuse.Cycles cycleCourant = laveuse.getCycle();
			switch(cycleCourant){
				case COTON:
					if(!boutonCoton.isSelected()){
						boutonCoton.setSelected(true);
					}
					break;
				case RUGUEUX:
					if(!boutonRugueux.isSelected()){
						boutonRugueux.setSelected(true);
					}
					break;
				case SYNTHETIQUE:
					if(!boutonSynthetique.isSelected()){
						boutonSynthetique.setSelected(true);
					}
					break;
				case DESINFECTION:
					if(!boutonDesinfection.isSelected()){
						boutonDesinfection.setSelected(true);
					}
					break;
				case TREMPAGE_ESSORAGE:
					if(!boutonTrempageEssorage.isSelected()){
						boutonTrempageEssorage.setSelected(true);
					}
					break;
			}
		}
	}
	
	private void changerNiveauEau(){
		if(laveuse.getEtat() == Laveuse.Etat.ARRETE){
			int niveauEau = laveuse.augmenterNiveauEau();
			progressBar.setValue(niveauEau);
			port.setNiveauEauVoulu(niveauEau);
		}
	}
	
	private void demarrer() {
		port.demarrer();
	}
	
	private void arreter() {
		port.arreter();
	}
	
	public void updateTemps(){
		int temps = port.getTempsRestant();
		labelTemps.setText(Integer.toString(temps));
	}
	
	public void updateEtape(Laveuse.Etape etape){
		switch(etape){
			case INACTIF:
				labelEtape.setText("Arr�t�e");
				break;
			case REMPLISSAGE:
				labelEtape.setText("Remplissage");
				break;
			case LAVAGE:
				labelEtape.setText("Lavage");
				break;
			case VIDANGE:
				labelEtape.setText("Vidange");
				break;
			case ESSORAGE:
				labelEtape.setText("Essorage");
				break;
			default:
				labelEtape.setText("Inconnu");
				break;
		}
	}
}
