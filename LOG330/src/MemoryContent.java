import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.DefaultFormatter;

import java.awt.FlowLayout;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;


public class MemoryContent extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JSpinner spinnerPompe, spinnerMoteur, spinnerFroid, spinnerChaud,
					 spinnerSavon, spinnerJavellisant, spinnerAssouplisseur,
					 spinnerConvertisseur, spinnerEau0, spinnerEau1,
					 spinnerEau2, spinnerEau3, spinnerDepart, spinnerCoton,
					 spinnerSynthetique, spinnerRugueux, spinnerDesinfection, 
					 spinnerTrempageEssorage, spinnerNiveauEau, 
					 spinnerTempsRestant, spinnerCapteurNiveau0,
					 spinnerCapteurNiveau1, spinnerCapteurNiveau2,
					 spinnerCapteurNiveau3;

	/**
	 * Create the frame.
	 */
	public MemoryContent() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 494, 360);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JPanel panel_27 = new JPanel();
		contentPane.add(panel_27);
		
		JPanel panel = new JPanel();
		panel_27.add(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		JLabel lblNewLabel = new JLabel("Adresses 100");
		lblNewLabel.setHorizontalAlignment(SwingConstants.LEFT);
		panel.add(lblNewLabel);
		
		JPanel panel_3 = new JPanel();
		panel.add(panel_3);
		
		JLabel lblNewLabel_3 = new JLabel("0 - Cuvette");
		panel_3.add(lblNewLabel_3);
		
		spinnerMoteur = new JSpinner();
		spinnerMoteur.setEnabled(false);
		spinnerMoteur.setModel(new SpinnerNumberModel(0, 0, 1, 1));
		panel_3.add(spinnerMoteur);
		
		JPanel panel_4 = new JPanel();
		panel.add(panel_4);
		
		JLabel lblPompe = new JLabel("1 - Pompe");
		panel_4.add(lblPompe);
		
		spinnerPompe = new JSpinner();
		spinnerPompe.setEnabled(false);
		spinnerPompe.setModel(new SpinnerNumberModel(1, 0, 1, 1));
		panel_4.add(spinnerPompe);
		
		JPanel panel_5 = new JPanel();
		panel.add(panel_5);
		
		JLabel lblEau_4 = new JLabel("2 - Eau Froide");
		panel_5.add(lblEau_4);
		
		spinnerFroid = new JSpinner();
		spinnerFroid.setEnabled(false);
		spinnerFroid.setModel(new SpinnerNumberModel(0, 0, 1, 1));
		panel_5.add(spinnerFroid);
		
		JPanel panel_6 = new JPanel();
		panel.add(panel_6);
		
		JLabel lblEau_5 = new JLabel("3 - Eau Chaude");
		panel_6.add(lblEau_5);
		
		spinnerChaud = new JSpinner();
		spinnerChaud.setEnabled(false);
		spinnerChaud.setModel(new SpinnerNumberModel(0, 0, 1, 1));
		panel_6.add(spinnerChaud);
		
		JPanel panel_7 = new JPanel();
		panel.add(panel_7);
		
		JLabel lblSavon = new JLabel("4 - Savon");
		panel_7.add(lblSavon);
		
		spinnerSavon = new JSpinner();
		spinnerSavon.setEnabled(false);
		spinnerSavon.setModel(new SpinnerNumberModel(0, 0, 1, 1));
		panel_7.add(spinnerSavon);
		
		JPanel panel_8 = new JPanel();
		panel.add(panel_8);
		
		JLabel lblJavellisant = new JLabel("5 - Javellisant");
		panel_8.add(lblJavellisant);
		
		spinnerJavellisant = new JSpinner();
		spinnerJavellisant.setEnabled(false);
		spinnerJavellisant.setModel(new SpinnerNumberModel(1, 0, 1, 1));
		panel_8.add(spinnerJavellisant);
		
		JPanel panel_10 = new JPanel();
		panel.add(panel_10);
		
		JLabel lblAssouplisseur = new JLabel("6 - Assouplisseur");
		panel_10.add(lblAssouplisseur);
		
		spinnerAssouplisseur = new JSpinner();
		spinnerAssouplisseur.setEnabled(false);
		spinnerAssouplisseur.setModel(new SpinnerNumberModel(0, 0, 1, 1));
		panel_10.add(spinnerAssouplisseur);
		
		JPanel panel_9 = new JPanel();
		panel.add(panel_9);
		
		JLabel lblConvertisseur = new JLabel("7 - Convertisseur A/N");
		panel_9.add(lblConvertisseur);
		
		spinnerConvertisseur = new JSpinner();
		spinnerConvertisseur.setEnabled(false);
		spinnerConvertisseur.setModel(new SpinnerNumberModel(0, 0, 1, 1));
		panel_9.add(spinnerConvertisseur);
		
		JPanel panel_1 = new JPanel();
		panel_27.add(panel_1);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.Y_AXIS));
		
		JLabel lblNewLabel_1 = new JLabel("Adresses 200");
		panel_1.add(lblNewLabel_1);
		
		JPanel panel_11 = new JPanel();
		panel_1.add(panel_11);
		
		JLabel lblEau = new JLabel("0 - Eau");
		lblEau.setHorizontalAlignment(SwingConstants.LEFT);
		panel_11.add(lblEau);
		
		spinnerEau0 = new JSpinner();
		spinnerEau0.setEnabled(false);
		spinnerEau0.setModel(new SpinnerNumberModel(0, 0, 1, 1));
		panel_11.add(spinnerEau0);
		
		JPanel panel_12 = new JPanel();
		panel_1.add(panel_12);
		
		JLabel lblEau_1 = new JLabel("1 - Eau");
		panel_12.add(lblEau_1);
		
		spinnerEau1 = new JSpinner();
		spinnerEau1.setEnabled(false);
		spinnerEau1.setModel(new SpinnerNumberModel(1, 0, 1, 1));
		panel_12.add(spinnerEau1);
		
		JPanel panel_13 = new JPanel();
		panel_1.add(panel_13);
		
		JLabel lblEau_2 = new JLabel("2 - Eau");
		panel_13.add(lblEau_2);
		
		spinnerEau2 = new JSpinner();
		spinnerEau2.setEnabled(false);
		spinnerEau2.setModel(new SpinnerNumberModel(0, 0, 1, 1));
		panel_13.add(spinnerEau2);
		
		JPanel panel_14 = new JPanel();
		panel_1.add(panel_14);
		
		JLabel lblEau_3 = new JLabel("3 - Eau");
		panel_14.add(lblEau_3);
		
		spinnerEau3 = new JSpinner();
		spinnerEau3.setEnabled(false);
		spinnerEau3.setModel(new SpinnerNumberModel(1, 0, 1, 1));
		panel_14.add(spinnerEau3);
		
		JPanel panel_15 = new JPanel();
		panel_1.add(panel_15);
		
		JLabel lblDpart = new JLabel("4 - D\u00E9part");
		panel_15.add(lblDpart);
		
		spinnerDepart = new JSpinner();
		spinnerDepart.setEnabled(false);
		spinnerDepart.setModel(new SpinnerNumberModel(0, 0, 1, 1));
		panel_15.add(spinnerDepart);
		
		JPanel panel_16 = new JPanel();
		panel_1.add(panel_16);
		
		JLabel lblCoton = new JLabel("5 - Coton");
		panel_16.add(lblCoton);
		
		spinnerCoton = new JSpinner();
		spinnerCoton.setEnabled(false);
		spinnerCoton.setModel(new SpinnerNumberModel(1, 0, 1, 1));
		panel_16.add(spinnerCoton);
		
		JPanel panel_17 = new JPanel();
		panel_1.add(panel_17);
		
		JLabel lblSynthetique = new JLabel("6 - Synth\u00E9tique");
		panel_17.add(lblSynthetique);
		
		spinnerSynthetique = new JSpinner();
		spinnerSynthetique.setEnabled(false);
		spinnerSynthetique.setModel(new SpinnerNumberModel(0, 0, 1, 1));
		panel_17.add(spinnerSynthetique);
		
		JPanel panel_18 = new JPanel();
		panel_1.add(panel_18);
		
		JLabel lblRugueux = new JLabel("7 - Rugueux");
		panel_18.add(lblRugueux);
		
		spinnerRugueux = new JSpinner();
		spinnerRugueux.setEnabled(false);
		spinnerRugueux.setModel(new SpinnerNumberModel(0, 0, 1, 1));
		panel_18.add(spinnerRugueux);
		
		JPanel panel_2 = new JPanel();
		panel_27.add(panel_2);
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.Y_AXIS));
		
		JLabel lblNewLabel_2 = new JLabel("Adresses 700");
		panel_2.add(lblNewLabel_2);
		
		JPanel panel_19 = new JPanel();
		panel_2.add(panel_19);
		
		JLabel lblDsinfection = new JLabel("0 - D\u00E9sinfection");
		panel_19.add(lblDsinfection);
		
		spinnerDesinfection = new JSpinner();
		spinnerDesinfection.setEnabled(false);
		spinnerDesinfection.setModel(new SpinnerNumberModel(0, 0, 1, 1));
		panel_19.add(spinnerDesinfection);
		
		JPanel panel_21 = new JPanel();
		panel_2.add(panel_21);
		
		JLabel labelCapteurNiveau3 = new JLabel("1 - Capteur de niveau");
		panel_21.add(labelCapteurNiveau3);
		
		spinnerCapteurNiveau3 = new JSpinner();
		spinnerCapteurNiveau3.setModel(new SpinnerNumberModel(0, 0, 1, 1));
//		spinnerCapteurNiveau3.addChangeListener(new ChangeListener(){
//			public void stateChanged(ChangeEvent e) {
//				updateNiveauEauValues();
//			}
//		});
		panel_21.add(spinnerCapteurNiveau3);
		
		JPanel panel_22 = new JPanel();
		panel_2.add(panel_22);
		
		JLabel labelCapteurNiveau2 = new JLabel("2 - Capteur de niveau");
		panel_22.add(labelCapteurNiveau2);
		
		spinnerCapteurNiveau2 = new JSpinner();
		spinnerCapteurNiveau2.setModel(new SpinnerNumberModel(0, 0, 1, 1));
//		spinnerCapteurNiveau2.addChangeListener(new ChangeListener(){
//			public void stateChanged(ChangeEvent e) {
//				updateNiveauEauValues();
//			}
//		});
		panel_22.add(spinnerCapteurNiveau2);
		
		JPanel panel_23 = new JPanel();
		panel_2.add(panel_23);
		
		JLabel labelCapteurNiveau1 = new JLabel("3 - Capteur de niveau");
		panel_23.add(labelCapteurNiveau1);
		
		spinnerCapteurNiveau1 = new JSpinner();
		spinnerCapteurNiveau1.setModel(new SpinnerNumberModel(0, 0, 1, 1));
//		spinnerCapteurNiveau1.addChangeListener(new ChangeListener(){
//			public void stateChanged(ChangeEvent e) {
//				updateNiveauEauValues();
//			}
//		});
		panel_23.add(spinnerCapteurNiveau1);
		
		JPanel panel_24 = new JPanel();
		panel_2.add(panel_24);
		
		JLabel labelCapteurNiveau0 = new JLabel("4 - Capteur de niveau");
		panel_24.add(labelCapteurNiveau0);
		
		spinnerCapteurNiveau0 = new JSpinner();
		spinnerCapteurNiveau0.setModel(new SpinnerNumberModel(0, 0, 1, 1));
//		spinnerCapteurNiveau0.addChangeListener(new ChangeListener(){
//			public void stateChanged(ChangeEvent e) {
//				updateNiveauEauValues();
//			}
//		});
		panel_24.add(spinnerCapteurNiveau0);
		
		JPanel panel_20 = new JPanel();
		panel_2.add(panel_20);
		
		JLabel lblNonUtilis_6 = new JLabel("5 - TrempageEssorage");
		panel_20.add(lblNonUtilis_6);
		
		spinnerTrempageEssorage = new JSpinner();
		spinnerTrempageEssorage.setEnabled(false);
		spinnerTrempageEssorage.setModel(new SpinnerNumberModel(0, 0, 1, 1));
		panel_20.add(spinnerTrempageEssorage);
		
		JPanel panel_25 = new JPanel();
		panel_2.add(panel_25);
		
		JLabel lblNonUtilis_1 = new JLabel("Non utilis\u00E9");
		panel_25.add(lblNonUtilis_1);
		
		JSpinner spinner_22 = new JSpinner();
		spinner_22.setEnabled(false);
		spinner_22.setModel(new SpinnerNumberModel(0, 0, 1, 1));
		panel_25.add(spinner_22);
		
		JPanel panel_26 = new JPanel();
		panel_2.add(panel_26);
		
		JLabel lblNonUtilis = new JLabel("Non utilis\u00E9");
		panel_26.add(lblNonUtilis);
		
		JSpinner spinner_23 = new JSpinner();
		spinner_23.setEnabled(false);
		spinner_23.setModel(new SpinnerNumberModel(0, 0, 1, 1));
		panel_26.add(spinner_23);
		
		JPanel panel_28 = new JPanel();
		contentPane.add(panel_28);
		
		JPanel panel_29 = new JPanel();
		panel_28.add(panel_29);
		
		JLabel lblNewLabel_4 = new JLabel("Niveau d'eau");
		panel_29.add(lblNewLabel_4);
		
		spinnerNiveauEau = new JSpinner();
		spinnerNiveauEau.setEnabled(false);
		spinnerNiveauEau.setModel(new SpinnerNumberModel(0, 0, 16, 1));
		panel_29.add(spinnerNiveauEau);
		
		JPanel panel_30 = new JPanel();
		panel_28.add(panel_30);
		
		JLabel lblNewLabel_5 = new JLabel("Temps restant");
		panel_30.add(lblNewLabel_5);
		
		spinnerTempsRestant = new JSpinner();
		spinnerTempsRestant.setModel(new SpinnerNumberModel(0, 0, 45, 1));
		panel_30.add(spinnerTempsRestant);
	}
	
	public int getValuePompe(){
		return (Integer) spinnerPompe.getValue();
	}
	
	public int getValueMoteur(){
		return (Integer) spinnerMoteur.getValue();
	}
	
	public int getValueFroid(){
		return (Integer) spinnerFroid.getValue();
	}
	
	public int getValueChaud(){
		return (Integer) spinnerChaud.getValue();
	}
	
	public int getValueSavon(){
		return (Integer) spinnerSavon.getValue();
	}
	
	public int getValueJavellisant(){
		return (Integer) spinnerJavellisant.getValue();
	}
	
	public int getValueAssouplisseur(){
		return (Integer) spinnerAssouplisseur.getValue();
	}
	
	public int getValueConvertisseur(){
		return (Integer) spinnerConvertisseur.getValue();
	}
	
	public int getValueEau0(){
		return (Integer) spinnerEau0.getValue();
	}
	
	public int getValueEau1(){
		return (Integer) spinnerEau1.getValue();
	}
	
	public int getValueEau2(){
		return (Integer) spinnerEau2.getValue();
	}
	
	public int getValueEau3(){
		return (Integer) spinnerEau3.getValue();
	}
	
	public int getValueCapteurNiveau0(){
		return (Integer) spinnerCapteurNiveau0.getValue();
	}
	
	public int getValueCapteurNiveau1(){
		return (Integer) spinnerCapteurNiveau1.getValue();
	}
	
	public int getValueCapteurNiveau2(){
		return (Integer) spinnerCapteurNiveau2.getValue();
	}
	
	public int getValueCapteurNiveau3(){
		return (Integer) spinnerCapteurNiveau3.getValue();
	}
	 
	public int getValueDepart(){
		return (Integer) spinnerDepart.getValue();
	}
	
	public int getValueCoton(){
		return (Integer) spinnerCoton.getValue();
	}
	
	public int getValueSynthetique(){
		return (Integer) spinnerSynthetique.getValue();
	}
	
	public int getValueRugueux(){
		return (Integer) spinnerRugueux.getValue();
	}
	
	public int getValueDesinfection(){
		return (Integer) spinnerDesinfection.getValue();
	}
	
	public int getValueTrempageEssorage() {
		return (Integer) spinnerTrempageEssorage.getValue();
	}

	public int getValueTempsRestant() {
		return (Integer) spinnerTempsRestant.getValue();
	}

	
	
	
	public void setValuePompe(Boolean value){
		if(value)
			spinnerPompe.setValue(1);
		else
			spinnerPompe.setValue(0);
	}
	
	public void setValueMoteur(Boolean value){
		if(value)
			spinnerMoteur.setValue(1);
		else
			spinnerMoteur.setValue(0);
	}
	
	public void setValueFroid(Boolean value){
		if(value)
			spinnerFroid.setValue(1);
		else
			spinnerFroid.setValue(0);
	}
	
	public void setValueChaud(Boolean value){
		if(value)
			spinnerChaud.setValue(1);
		else
			spinnerChaud.setValue(0);
	}
	
	public void setValueSavon(Boolean value){
		if(value)
			spinnerSavon.setValue(1);
		else
			spinnerSavon.setValue(0);
	}
	
	public void setValueJavellisant(Boolean value){
		if(value)
			spinnerJavellisant.setValue(1);
		else
			spinnerJavellisant.setValue(0);
	}
	
	public void setValueAssouplisseur(Boolean value){
		if(value)
			spinnerAssouplisseur.setValue(1);
		else
			spinnerAssouplisseur.setValue(0);
	}
	
	public void setValueConvertisseur(Boolean value){
		if(value)
			spinnerConvertisseur.setValue(1);
		else
			spinnerConvertisseur.setValue(0);
	}
	
	public void setValueEau0(Boolean value){
		if(value)
			spinnerEau0.setValue(1);
		else
			spinnerEau0.setValue(0);
	}
	
	public void setValueEau1(Boolean value){
		if(value)
			spinnerEau1.setValue(1);
		else
			spinnerEau1.setValue(0);
	}
	
	public void setValueEau2(Boolean value){
		if(value)
			spinnerEau2.setValue(1);
		else
			spinnerEau2.setValue(0);
	}
	
	public void setValueEau3(Boolean value){
		if(value)
			spinnerEau3.setValue(1);
		else
			spinnerEau3.setValue(0);
	}
	 
	public void setValueDepart(Boolean value){
		if(value)
			spinnerDepart.setValue(1);
		else
			spinnerDepart.setValue(0);
	}
	
	public void setValueCoton(Boolean value){
		if(value)
			spinnerCoton.setValue(1);
		else
			spinnerCoton.setValue(0);
	}
	
	public void setValueSynthetique(Boolean value){
		if(value)
			spinnerSynthetique.setValue(1);
		else
			spinnerSynthetique.setValue(0);
	}
	
	public void setValueRugueux(Boolean value){
		if(value)
			spinnerRugueux.setValue(1);
		else
			spinnerRugueux.setValue(0);
	}
	
	public void setValueDesinfection(Boolean value){
		if(value)
			spinnerDesinfection.setValue(1);
		else
			spinnerDesinfection.setValue(0);
	}
	
	public void setValueTrempageEssorage(Boolean value){
		if(value)
			spinnerTrempageEssorage.setValue(1);
		else
			spinnerTrempageEssorage.setValue(0);
	}
	
	public void setValueTempsRestant(int value){
		SpinnerNumberModel model = (SpinnerNumberModel)spinnerTempsRestant.getModel(); 
		if(model.getMaximum().compareTo(value) == -1){
			model.setMaximum(value);
		}
		model.setValue(value);
	}

	private void updateNiveauEauValues(){
		
		int niveau = 0;
		
		niveau += getValueCapteurNiveau0();
		niveau += getValueCapteurNiveau1()*10;
		niveau += getValueCapteurNiveau2()*100;
		niveau += getValueCapteurNiveau3()*1000;
		
		String niveauStr = Integer.toString(toBinary(niveau));
		
		spinnerNiveauEau.setValue(Integer.parseInt(niveauStr,2));
		
	}
	
	private static int toBinary(int n)
	{
	        String b = ""; // binary representation as a string
	        while (n != 0) {
	            int r = (int)(n % 2); // remainder
	            b = r + b; // concatenate remainder
	            n /= 2; // reduce n
	        }
	        return Integer.parseInt(b);
	}
	
}
