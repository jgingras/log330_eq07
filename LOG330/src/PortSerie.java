
public class PortSerie {
	
	MemoryContent memContent;
	
	public PortSerie(MemoryContent memContent) {
		this.memContent =  memContent;
	}
	
	public void ouvrirValveSavon() {
		System.out.println("Ouvre le savon");
		memContent.setValueSavon(true);
	}
	
	public void ouvrirValveJavellisant() {
		System.out.println("Ouvre le javellisant");
		memContent.setValueJavellisant(false);
	}
	
	public void ouvrirValveAssouplisseur() {
		System.out.println("Ouvre l'asouplisseur");
		memContent.setValueAssouplisseur(true);
	}
	
	public void fermerValveSavon() {
		System.out.println("Fermer le savon");
		memContent.setValueSavon(false);
	}
	
	public void fermerValveJavellisant() {
		System.out.println("Fermer le javellisant");
		memContent.setValueJavellisant(true);
	}
	
	public void fermerValveAssouplisseur() {
		System.out.println("Fermer l'assouplisseur");
		memContent.setValueAssouplisseur(false);
	}
	
	public void setCadenceMoteur(int cadence) {
		if(cadence > 0){
			System.out.println("D�marre le moteur de rotation.");
			memContent.setValueMoteur(true);
		}
		else
		{
			System.out.println("Arr�te le moteur de rotation.");
			memContent.setValueMoteur(false);
		}
	}
	
	public void ouvrirEauChaude(){
		System.out.println("Ouvre l'eau chaude.");
		memContent.setValueChaud(true);
	}
	
	public void ouvrirEauFroide(){
		System.out.println("Ouvre l'eau froide.");
		memContent.setValueFroid(true);
	}
	
	public void fermerEauFroide(){
		System.out.println("Ferme l'eau froide.");
		memContent.setValueFroid(false);
	}
	
	public void fermerEauChaude(){
		System.out.println("Ferme l'eau chaude.");
		memContent.setValueChaud(false);
	}
	
	public void demarrerPompe(){
		System.out.println("Demarre la pompe de vidange.");
		memContent.setValuePompe(false);
	}
	
	public void arreterPompe(){
		System.out.println("Arr�te la pompe de vidange.");
		memContent.setValuePompe(true);
	}
	
	public void setCycle(Laveuse.Cycles cycle){

		switch(cycle){
			case COTON:
				System.out.println("Set le cycle � coton.");
				memContent.setValueCoton(true);
				memContent.setValueRugueux(false);
				memContent.setValueSynthetique(false);
				memContent.setValueDesinfection(false);
				memContent.setValueTrempageEssorage(false);
				break;
			case SYNTHETIQUE:
				System.out.println("Set le cycle � synth�tique.");
				memContent.setValueCoton(false);
				memContent.setValueRugueux(false);
				memContent.setValueSynthetique(true);
				memContent.setValueDesinfection(false);
				memContent.setValueTrempageEssorage(false);
				break;
			case RUGUEUX:
				System.out.println("Set le cycle � rugueux.");
				memContent.setValueCoton(false);
				memContent.setValueRugueux(true);
				memContent.setValueSynthetique(false);
				memContent.setValueDesinfection(false);
				memContent.setValueTrempageEssorage(false);
				break;
			case TREMPAGE_ESSORAGE:
				System.out.println("Set le cycle � trempage/essorage.");
				memContent.setValueCoton(false);
				memContent.setValueRugueux(false);
				memContent.setValueSynthetique(false);
				memContent.setValueDesinfection(false);
				memContent.setValueTrempageEssorage(true);
				break;
			case DESINFECTION:
				System.out.println("Set le cycle � d�sinfection.");
				memContent.setValueCoton(false);
				memContent.setValueRugueux(false);
				memContent.setValueSynthetique(false);
				memContent.setValueDesinfection(true);
				memContent.setValueTrempageEssorage(false);
				break;
		}
	}

	public Laveuse.Cycles getCycle(){	
		if(memContent.getValueCoton() == 1) {
			System.out.println("Get le cycle coton.");
			memContent.setValueRugueux(false);
			memContent.setValueSynthetique(false);
			memContent.setValueDesinfection(false);
			memContent.setValueTrempageEssorage(false);
			return Laveuse.Cycles.COTON;
		}
		else if(memContent.getValueSynthetique() == 1){
			System.out.println("Get le cycle synth�tique.");
			memContent.setValueRugueux(false);
			memContent.setValueDesinfection(false);
			memContent.setValueTrempageEssorage(false);
			return Laveuse.Cycles.SYNTHETIQUE;
		}
		else if(memContent.getValueRugueux() == 1){
			System.out.println("Get le cycle rugueux.");
			memContent.setValueDesinfection(false);
			memContent.setValueTrempageEssorage(false);
			return Laveuse.Cycles.RUGUEUX;
		}
		else if(memContent.getValueDesinfection() == 1){
			System.out.println("Get le cycle d�sinfectant.");
			memContent.setValueTrempageEssorage(false);
			return Laveuse.Cycles.DESINFECTION;
		}
		else if(memContent.getValueTrempageEssorage() == 1){
			System.out.println("Get le cycle Trempage/Essorage.");
			return Laveuse.Cycles.TREMPAGE_ESSORAGE;
		}
		else{
			System.out.println("Get auxun cycle.");
			memContent.setValueCoton(true);
			return Laveuse.Cycles.COTON;
		}
	}
	
	public int getNiveauEauVoulu(){

		int niveau = 0;
		
		niveau += memContent.getValueEau0();
		niveau += memContent.getValueEau1()*10;
		niveau += memContent.getValueEau2()*100;
		niveau += memContent.getValueEau3()*1000;
		
		String niveauStr = Integer.toString(niveau);
		
		
		return Integer.parseInt(niveauStr,2);
	}
	
	public void setNiveauEauVoulu(int niveau){
		System.out.println("Set le niveau d'eau � " + niveau + ".");
		int niveauBinaire = toBinary(niveau);
		String niveauStr = Integer.toString(niveauBinaire);
		
		if(niveauStr.length()>0 && Integer.parseInt(niveauStr.substring(niveauStr.length()-1)) == 1){
			memContent.setValueEau0(true);
		}
		else
		{
			memContent.setValueEau0(false);
		}
		
		if(niveauStr.length()>1 && Integer.parseInt(niveauStr.substring(niveauStr.length()-2,niveauStr.length()-1)) == 1){
			memContent.setValueEau1(true);
		}
		else
		{
			memContent.setValueEau1(false);
		}
		
		if(niveauStr.length()>2 && Integer.parseInt(niveauStr.substring(niveauStr.length()-3,niveauStr.length()-2)) == 1){
			memContent.setValueEau2(true);
		}
		else
		{
			memContent.setValueEau2(false);
		}

		if(niveauStr.length()>3 && Integer.parseInt(niveauStr.substring(niveauStr.length()-4,niveauStr.length()-3)) == 1){
			memContent.setValueEau3(true);
		}
		else
		{
			memContent.setValueEau3(false);
		}

	}

	public int getCurrentNiveauEau(){
		System.out.println("Get le niveau d'eau.");
		int niveau = 0;
		
		niveau += memContent.getValueCapteurNiveau0();
		niveau += memContent.getValueCapteurNiveau1()*10;
		niveau += memContent.getValueCapteurNiveau2()*100;
		niveau += memContent.getValueCapteurNiveau3()*1000;
		
		String niveauStr = Integer.toString(niveau);
		
		return Integer.parseInt(niveauStr,2);
	}
	
	public Boolean estEnMarche(){
		return memContent.getValueDepart() == 1;
	}
	
	public void demarrer(){
		System.out.println("D�marre la laveuse.");
		memContent.setValueDepart(true);
	}
	
	public void arreter(){
		System.out.println("Arr�te la laveuse.");
		memContent.setValueDepart(false);
	}
	
	public int getTempsRestant(){
		return memContent.getValueTempsRestant();
	}
	
	public void setTempsRestant(int temps){
		memContent.setValueTempsRestant(temps);
	}
	
	private static int toBinary(int n)
	{
	        String b = ""; // binary representation as a string
	        while (n != 0) {
	            int r = (int)(n % 2); // remainder
	            b = r + b; // concatenate remainder
	            n /= 2; // reduce n
	        }
	        return Integer.parseInt(b);
	}

}
